DROP TABLE IF EXISTS cards;
DROP TABLE IF EXISTS texts;
DROP TABLE IF EXISTS users;

CREATE TABLE users (
  id            BIGINT IDENTITY,
  first_name    VARCHAR(255),
  last_name     VARCHAR(255),
  login         VARCHAR(255),
  password_hash VARCHAR(255),
  PRIMARY KEY (id)
);

CREATE TABLE cards (
  id          BIGINT IDENTITY,
  minded      BOOLEAN NOT NULL,
  translation VARCHAR(255),
  word        VARCHAR(255),
  user_id     BIGINT,
  PRIMARY KEY (id)
);
CREATE TABLE texts (
  id            BIGINT IDENTITY,
  document_text VARCHAR(2000),
  topic         VARCHAR(255),
  user_id       BIGINT,
  PRIMARY KEY (id)
);

ALTER TABLE cards
  ADD CONSTRAINT FK_cards_users FOREIGN KEY (user_id) REFERENCES users;
ALTER TABLE texts
  ADD CONSTRAINT FK_texts_users FOREIGN KEY (user_id) REFERENCES users;
ALTER TABLE cards
  ADD CONSTRAINT unique_word UNIQUE (user_id, word);